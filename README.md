# drug-management-vue

> a Drug manageSystem (毕设)

## Build Setup

``` bash
# Clone project
git clone https://github.com/Shenlianxi/vue-drag-management-system

# Install dependencies
npm install --registry=https://registry.npm.taobao.org

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```